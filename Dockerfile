FROM cmssw/el8:x86_64

WORKDIR /app

COPY ./* /app

RUN curl -fsSL https://get.htcondor.org | /bin/bash -s -- --no-dry-run 
RUN yum install -y vim-enhanced perl-core perl-CPAN perl-Sys-Syslog perl-Authen-Krb5 sssd-client
RUN git clone https://github.com/cms-sw/genproductions.git --depth=1

ENTRYPOINT ["/app/setupCondor.sh"]
# Run app.py when the container launches
CMD ["/bin/bash"]

WORKDIR "genproductions/bin/MadGraph5_aMCatNLO/"
ENV WORKDIR="genproductions/bin/MadGraph5_aMCatNLO/"
